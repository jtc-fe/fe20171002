function cong(a, b) {
    return a + b;
}

    console.log(document.getElementById("ketqua"));

function tru(a, b) {
    return a - b;
}

function nhan(a, b) {
    return a * b;
}

function chia(a, b) {
    return a / b;
}

function onclickCong() {
    // lay gia tri cua 2 input
    var so1 = parseInt(document.getElementById("so1").value);
    var so2 = parseInt(document.getElementById("so2").value);

    if (isNaN(so1) || isNaN(so2)) {
        document.getElementById("ketqua").innerHTML = "Bạn phải nhập vào số";
        return;
    }

    // xu ly gia tri: cong 2 gia tri
    var ketqua = cong(so1, so2);
    // show ket qua ra ngoai html
    document.getElementById("ketqua").innerHTML = "Kết quả của phép cộng là: " + ketqua;
}

function submitForm() {
    alert('form submitted');
}

function chuotDuocDiVao() {
    document.getElementById("demo_mouse").innerHTML = "Vui quá!!!";

    document.getElementById("mouse").style["background-color"] = "red";
    document.getElementById("mouse").style.color = "white";

    document.getElementById("mouse").className += " class_3";
}

function chuotDiRaNgoai() {
    document.getElementById("demo_mouse").innerHTML = "Buồn quá!!!";
    document.getElementById("mouse").style["background-color"] = "green";
    document.getElementById("mouse").style.color = "black";
}

function textFocus() {
    console.log('123');
}

function textBlur() {
    console.log('blur');
}


function addClass() {
    document.getElementById("div1").classList.add("background_red");
}

function removeClass() {
    document.getElementById("div1").classList.remove("background_red");
}


function onClickBtnMenu() {
    var menu = document.getElementById("menu");
    if (menu.classList.contains("menu_overlap")) {
        menu.classList.remove("menu_overlap");
    } else {
        menu.classList.add("menu_overlap");
    }
}

function demoAlert() {
    alert("ahihih");
}

function demoPromt() {
    var yourname = prompt('Ten ban la gi: ');
    console.log('yourname: ', yourname);
}

function demoConfirm() {
    var isConfirm = confirm("Bạn có chắc không?");
    console.log('is confirm: ', isConfirm);
}

var chuoi1 = "ten toi la Tuan Anh";
var chuoi2 = "toi 25 tuoi";

// concat
var kq = chuoi1.concat(chuoi2); 
console.log(kq);
console.log(chuoi1 + chuoi2);

// indexOf
var index = chuoi1.indexOf('t');
console.log('indexOf:', index);

// lastIndexOf
var lastIndex = chuoi1.lastIndexOf('t');
console.log('last index:', lastIndex);

// charAt
var char = chuoi1.charAt(6);
console.log(char);

// split
var array = chuoi1.split(" ");
console.log('split: ', array);

// slice
console.log('slice', chuoi1.slice(3, 7)); // 3,4,5,6



// ARRAY
var array1 = [1,2,3,4,5];
var array2 = [2,1,6,7];

// concat
var array3 = array1.concat(array2);
console.log('array concat', array3);

// push
array1.push(8);
console.log('push', array1);

// pop
// 1,2,3,4,5,8
var a = array1.pop();
console.log('pop', a, array1);

// reverse
var reverse = array1.reverse();
console.log('reverse', reverse);

// NUMBER
var pInt = parseInt('12.5678');
console.log('parseInt', pInt);
var pFloat = parseFloat('12.5678');
console.log('parseFloat', pFloat);

// DATE
var current = new Date();
console.log(current);
console.log(current.getDate())
console.log(current.getDay())

current.setMonth(0);
console.log(current);
console.log(current.toISOString());




// JQUERY
$(document).ready(function() {
    // Cach 1
    $("#myid").css("background-color", "red");
    $("#myid").css("color", "white");

    // Cach 2
    $("#myid").css({
        "background-color": "red",
        "color": "white"
    });

    $('#menu li:eq(1)').css({
        "color": "red"
    });

    // $('body').append("<div> <h1>JQuery Core</h1> </div>");
    console.log($('ul li')[0].firstChild.data);
    var li = $('ul li:first-child')[0].innerText;
    console.log(li);

    // $('body'). = "<div> <h1>JQuery Core</h1> </div>";
})